//
//  Constants.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation


enum Constants {
    
    enum Messages {
        static let error = "error al cargar la información"
    }

    enum NibNames {
        static let stepOneCell = "StepOneCell"
    }
    enum Helpers {
        static let noImage = "NoImage"
    }
}
