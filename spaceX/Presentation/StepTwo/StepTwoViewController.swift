//
//  StepTwoViewController.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import UIKit
import WebKit

class StepTwoViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let url = URL(string: "https://www.google.com.pe/?hl=es") else {return}
        webView.load(URLRequest(url:url ))
        // Do any additional setup after loading the view.
    }
    
    func setupUI(title: String) {
        self.title = title
    }

}
