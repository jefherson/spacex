//
//  StepOneCell.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import UIKit

class StepOneCell: UITableViewCell {

    @IBOutlet weak var imageLaunch: UIImageView!
    @IBOutlet weak var missionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var rocketLabel: UILabel!
    @IBOutlet weak var statusView: UIViewCustom!
    
    static let identifier = "StepOneCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var data : LaunchesModel! {
        didSet  {
            imageLaunch.image = UIImage(named: Constants.Helpers.noImage)
            missionLabel.text = data.missionName
            dateLabel.text = "\(data.date ?? 0)"
            rocketLabel.text = data.rocket
            buildStatusView()
        }
    }

    private func buildStatusView() {
        switch data.status {
        case .success :
            statusView.backgroundColor = UIColor.green
        case .fail:
            statusView.backgroundColor = UIColor.red
        case .none:
            statusView.backgroundColor = UIColor.clear
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
