//
//  StepOneViewModel.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation

protocol StepOneViewModelProtocol {
        
    // properties
    var spaceXModel: SpaceXModel {get set}

    
    // OutBound
    var updateDescription: ObservableCall<String> { get set }
    var updateTableView: ObservableCall<Bool>{ get set }
    // Inbound
    func loadData()
    func orderLaunches()
}

class StepOneViewModel: StepOneViewModelProtocol {
    
    // properties
    var spaceXModel = SpaceXModel()
    
    // OutBound
    var updateDescription = ObservableCall<String>()
    var updateTableView = ObservableCall<Bool>()

    // Inbound
    func loadData() {
    
        SpaceXManager.shared.getDataLaunches { (spaceXModel) in
            self.spaceXModel = spaceXModel
            self.buildDescription(data: spaceXModel)
        } failure: { (error) in
            print(error)
        }
    }
    
    func orderLaunches() {

        let sortedLaunches = spaceXModel.launches?.sorted {
            $0.date ?? 0 < $1.date ?? 0
        }
        
        spaceXModel.launches?.removeAll()
        spaceXModel.launches = sortedLaunches
        updateTableView.notify(with: true)
    }
    
    
    private func buildDescription(data: SpaceXModel) {
        let message = "\(data.companyName ?? "") was founded by \(data.founderName ?? "") in \(data.year ?? ""). It has now \(data.employees ?? "") employees,\(data.launchSites ?? "") launch sites, and is valued at USD \(data.valuation ?? "")"
        updateDescription.notify(with: message)
    }
}
