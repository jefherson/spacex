//
//  StepOneViewController.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import UIKit


class StepOneViewController: UIViewController {

    var viewModel: StepOneViewModelProtocol = StepOneViewModel()

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var launchesTitle: UILabel!
    @IBOutlet weak var launchesTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBuilding()
        viewModel.loadData()
        setupUI()
    }

    private func setupUI() {
        let nib = UINib(nibName: Constants.NibNames.stepOneCell, bundle: nil)
        launchesTableView.register(nib, forCellReuseIdentifier: StepOneCell.identifier)
        launchesTableView.delegate = self
        launchesTableView.dataSource = self
    }
    
    private func setupBuilding() {
        viewModel.updateDescription.bind(to: self) { (self, message) in
            self.descriptionLabel.text = message
        }
        
        viewModel.updateTableView.bind(to: self) { (self, flag) in
            self.launchesTableView.reloadData()
        }
    }
    @IBAction func filterTap(_ sender: Any) {
        viewModel.orderLaunches()
    }
}

extension StepOneViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.spaceXModel.launches?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StepOneCell.identifier, for: indexPath) as? StepOneCell else {
            return UITableViewCell();
        }
        cell.data = viewModel.spaceXModel.launches?[indexPath.row]
        return cell
    }
}

extension StepOneViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController: StepTwoViewController = StepTwoViewController.instantiate()
        viewController.setupUI(title: viewModel.spaceXModel.launches?[indexPath.row].missionName ?? "")
        navigationController?.pushViewController(viewController, animated: true)
    }
}
