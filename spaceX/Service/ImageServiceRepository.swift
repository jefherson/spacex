//
//  ImageServiceRepository.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation
import UIKit

class ImageServiceRepository: RestApi {
    
    // MARK: - getImage
    func getImage(withUrl url : String,
                  success: @escaping (UIImage?) -> Void,
                  failure: @escaping (Error) -> Void) {
        
        requestImage(url: url, success: success, failure: failure)
        
    }
}
