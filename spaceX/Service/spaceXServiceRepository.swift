//
//  spaceXRepository.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation

class spaceXServiceRepository: RestApi {
    
    func getDataLaunches(success: @escaping (SpaceXModel) -> Void,
                     failure: @escaping (Error) -> Void) {
        
        let url = "https://api.spacexdata.com/v5/launches/latest"
        
        request(url: url, verb: .get, success: success, failure: failure)
    }
}
