//
//  RestApi.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation
import UIKit

public enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

protocol RestApi {
   
}

extension RestApi {
    
    var baseURL:String {
        if let url = Bundle.main.object(forInfoDictionaryKey: "Constants.urlkey") as? String {
            return url
        }
        return ""
    }
    
    var headers: [String : String] {
        let token = "Bearer jnasdpnajsdahdxzccxzc"
        
        return ["Content-Type":"Application/json",
                "Authorization": token]
        
    }
    
    func request(url: String,
                 verb: HTTPMethod,
                 parameters: [String:Any]? = nil,
                 headers:[String:String]? = nil,
                 success: @escaping (SpaceXModel) -> Void,
                 failure: @escaping (Error) ->Void) {
        
        executeRequest(url: url, verb: verb, parameters: parameters, headers: headers, success: success, failure: failure)
    
    }
    
    func requestImage(url:String,
                      headers:[String:String]? = nil,
                      success: @escaping (UIImage?) -> Void,
                      failure: @escaping (Error) -> Void) {
        
        let serviceHeaders = headers ?? self.headers
        
//        request(url, method: .get, headers: serviceHeaders ).responseImage { response in
//
//            debugPrint(response)
//
//            if let image = response.result.value {
//                success(image)
//                print("image downloaded: \(image)")
//            }
//        }
    }

    private func executeRequest(url:String,
                                verb:HTTPMethod,
                                parameters:[String:Any]? = nil,
                                headers:[String:String]? = nil,
                                success: @escaping (SpaceXModel) -> Void,
                                failure: @escaping (Error) -> Void) {
       
        let serviceHeaders = headers ?? self.headers
        
        success(createModel())
        
    }
    
    private func createModel() -> SpaceXModel{

        let launche1 = LaunchesModel(missionName: "Alcanzar la órbita",
                                     date: 2008,
                                     rocket: "Falcon 1",
                                     status: .success)
        let launche2 = LaunchesModel(missionName: "Orbitar la tierra",
                                     date: 2010,
                                     rocket: "Falcon 3",
                                     status: .fail)
        
        let launche3 = LaunchesModel(missionName: "Primer aterrizaje propulsado",
                                     date: 2015,
                                     rocket: "Falcon 9",
                                     status: .success)
        
        let launche4 = LaunchesModel(missionName: "Primera reutilización de un cohete orbital",
                                     date: 2017,
                                     rocket: "Falcon 9",
                                     status: .success)
        
        let launche5 = LaunchesModel(missionName: "Viaje a la luna",
                                     date: 2024,
                                     rocket: "Falcon 9",
                                     status: .none)
        
        let launche6 = LaunchesModel(missionName: "Viaje a la marte",
                                     date: 2025,
                                     rocket: "Dragon 2",
                                     status: .none)
        
        var launches = [LaunchesModel]()
        launches.append(launche3)
        launches.append(launche5)
        launches.append(launche1)
        launches.append(launche4)
        launches.append(launche2)
        launches.append(launche6)
        
        let spaceXModel = SpaceXModel(companyName: "SpaceX",
                                      founderName: "Elon Musk",
                                      year: "2002",
                                      employees: "3300",
                                      launchSites: "eeuu",
                                      valuation: "13.5 bk $",
                                      launches: launches)
        
        return spaceXModel
    }
}

