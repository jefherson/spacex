//
//  SpaceXManager.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation

class SpaceXManager {
    
    // MARK: - Singleton
    static let shared = SpaceXManager()
    
    // MARK: - SERVICE
    let service = spaceXServiceRepository()
        
    // MARK: - GetCountries
    func getDataLaunches(success: @escaping (SpaceXModel) -> Void,
                      failure: @escaping (Error) -> Void) {
        
        service.getDataLaunches ( success: { (serviceResponse) in
            
            success(serviceResponse)
            
        } , failure: failure)
    }
}
