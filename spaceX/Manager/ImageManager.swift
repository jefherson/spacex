//
//  ImageManager.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation
import UIKit

class ImageManager {
    
    // MARK: - Singleton
    static let shared = ImageManager()
    
    let service = ImageServiceRepository()
    
    // MARK: - getImage
    func getImage(withUrl url: String,
                  success: @escaping (UIImage?) -> Void,
                  failure: @escaping (Error) -> Void) {
        
        service.getImage(withUrl: url, success: { (serviceResponse) in
            success(serviceResponse)
            
        }, failure: failure)
        
    }
}
