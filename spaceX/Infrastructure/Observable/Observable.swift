//
//  Observable.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation
import UIKit

public struct ObservableCall<Input> {
    
    private var callBack: ((Input)-> Void)?
    
    public mutating func bind<Observer: AnyObject>(to observer: Observer, with callback: @escaping (Observer, Input)-> Void) {
        self.callBack = { [weak observer] input in
            guard let observer = observer else {
                return
            }
            callback(observer, input)
        }
    }
    
    public func notify(with input: Input) {
        callBack?(input)
    }
    
    public init() {}
}

