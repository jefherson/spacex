//
//  String+Extension.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation

extension String {
    
    func text(before text: String) -> String? {
        guard let range = self.range(of: text) else { return nil }
        return String(self[self.startIndex..<range.lowerBound])
    }
}
