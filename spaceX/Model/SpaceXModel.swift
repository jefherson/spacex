//
//  SpaceXModel.swift
//  spaceX
//
//  Created by BCP MAC on 3/05/23.
//

import Foundation

enum statusLaunch {
    case success
    case fail
    case none
}

struct SpaceXModel {
    
    var companyName: String?
    var founderName: String?
    var year: String?
    var employees: String?
    var launchSites: String?
    var valuation: String?
    var launches: [LaunchesModel]?
    
//    let fairings: String?
//    let links: LinksModel
//    let static_fire_date_utc: String
//    let static_fire_date_unix: Int
//    let tdb: Bool
//    let net: Bool
//    let window: Int
//    let rocket: String
//    let success: Bool
//    let failures: [String]
//    let details: String
//    let crew: [String]
//    let ships: [String]
//    let capsules: [String]
//    let payloads: [String]
//    let launchpad: String
//    let auto_update: Bool
//    let flight_number: Int
//    let name: String
//    let date_utc: String
//    let date_unix: Int
//    let date_local: String
//    let date_precision:String
//    let upcoming: Bool
//    let cores: [CoresModel]
//    let id: String
}

struct LaunchesModel {
    var missionName: String?
    var date: Int?
    var rocket: String?
    var status: statusLaunch
}
//struct LinksModel: Codable {
//    let patch: PatchModel
//    let reddit: RedditModel
//    let flickr: FlickrModel
//    let presskit: String
//    let webcast: String
//    let youtube_id: String
//    let article: String
//    let wikipedia: String
//}
//
//struct PatchModel: Codable {
//    let small: String
//    let large: String
//}
//
//struct RedditModel: Codable {
//    let campaign: String
//    let launch: String
//    let media: String
//    let recovery: String
//}
//
//struct FlickrModel: Codable {
//    let small: [String]
//    let original: [String]
//}
//
//struct CoresModel: Codable {
//    let core: String
//    let flight: Int
//    let gridfins: Bool
//    let legs: Bool
//    let reused: Bool
//    let landing_attempt: Bool
//    let landing_success: Bool
//    let landing_type: String
//    let landpad: String
//}
